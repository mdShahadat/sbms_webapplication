<span class="underline">Web Application Development using ASP.Net
MVC</span>

**<span class="underline">Project : Small Business Management
System</span>**

Deadline: 31/10/2019

<span class="underline">Resources</span>:

  - Md Shahadat Hossain
    
    Apprentice Software Engineer  
    Enlight Solutions

<span class="underline">Release features:</span>

1.  Product Catalog Setup
    
    1.  **Product Category Setup**
    
    <!-- end list -->
    
      - Initially show available records
    
      - Data can be added or modified any time
    
      - Each data (Name, Code) must be uniquely identified (unique)
    
      - Must provide all data (Name, Code)
    
      - Code must be 4 characters in length
    
      - User can search by Name, Code
    
    <!-- end list -->
    
    2.  **Product Setup with Category**
    
    <!-- end list -->
    
      - Initially show available records
    
      - Data can be added or modified any time
    
      - Each data (Name, Code) must be uniquely identified (unique)
    
      - Must provide all data (Category, Name, Code, Reorder Level)
    
      - Code must be 4 characters in length
    
      - Initially Reorder Level should be 10 or as per user (but only
        positive number allowed)
    
      - User can search by Category, Name and Code

2\. Party Module

**a. Customer Entry, Edit, Search**

  - Initially show available records

  - Data can be added or modified any time

  - Code, Contact, Email must be uniquely identified (unique)

  - Must provide Code, Name, Contact and Email

  - Code must be 4 characters in length

  - User can search by Name, Contact and Email

**b. Supplier Entry, Edit, Search**

  - Initially show available records

  - Data can be added or modified any time

  - Code, Contact, Email must be uniquely identified (unique)

  - Must provide Code, Name, Contact and Email

  - Code must be 4 characters in length

  - User can search by Name, Contact and Email

Git: <https://gitlab.com/mdShahadat/sbms_webapplication.git>

![](Image/media/image1.png)
