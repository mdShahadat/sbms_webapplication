<span class="underline">Web Application Development using ASP.Net
MVC</span>

**<span class="underline">Project: Small Business Management
System</span>**

Release Date: 14/11/2019

**<span class="underline">Resources</span>:**

  - Md Shahadat Hossain
    
    Apprentice Software Engineer  
    Enlight Solutions

**<span class="underline">Release Features:</span>**

1.  Purchase Module
    
      - Initially show available records
    
      - Data can be added or modified any before submit
    
      - \[MRP Calculation Formula: MRP = Unit Cost + 25% of Unit Cost\]
    
      - On purchase (Submit) stock quantity will be increased
    
      - Product is selected as per Category

2.  Sales Module
    
      - Data can be added or modified until it is submitted
    
      - Stock quantity will be decreased on sales
    
      - On purchase price customer will gets loyalty point
    
      - \[Discount (%) Calculation Formula: Loyalty Point/10 and Reset
        Loyalty Point\]
    
      - \[Reset Loyalty Point: Loyalty Point - Loyalty Point/10\]
    
      - User will be notified if quantity goes below the Reorder level

3.  Stock Module
    
      - Periodical Stock Report (From - To, Opening Balance, In, Out,
        Closing Balance)
    
      - By default, Low Reorder Level product & closely expired product
        will be shown on top

4.  Reporting Module
    
      - Periodic profit on sales
    
      - Periodic profit on purchased items (Available Item)

> **<span class="underline">Pending Features:</span>**

User can search by Code, Date and View details

Product should be sold by following FIFO (First in First out) method.

Form validation message show

Git: https://gitlab.com/mdShahadat/sbms\_webapplication.git
