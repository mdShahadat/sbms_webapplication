<span class="underline">Web Application Development using ASP.Net
MVC</span>

**<span class="underline">Project : Small Business Management
System</span>**

Deadline: 05/11/2019

<span class="underline">Resources</span>:

  - Md Shahadat Hossain
    
    Apprentice Software Engineer  
    Enlight Solutions

<span class="underline">Release features:</span>

1.  Product Catalog Setup
    
    1.  > **Product Category Setup**

> Form validation- using ajax

2.  > **Product Setup with Category**

> Initially Reorder Level should be 10 or as per user (but only positive
> number allowed)  
> Form validation using ajax

2\. Party Module

**a. Customer Entry, Edit, Search**

> Initially Loyalty Point should be 0 or as per user provides
> 
> Form validation using ajax

**b. Supplier Entry, Edit, Search**

> Form validation using ajax

Git: https://gitlab.com/mdShahadat/sbms\_webapplication.git
